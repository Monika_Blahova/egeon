require 'spec_helper'
require 'navigation_helper'

describe "personalities" do
  before(:each) do
    visit('/')
    fill_in 'user_email', :with => 'jack@example.com'
    fill_in 'user_password', :with => 'password'
    click_on 'log_in'
    page.should have_content 'Jack Doe'
  end

  it "can be created by editors" do
    create_personality_document('Name', 'Surname')
    click_action_link :drafts
    page.should have_content 'Surname, Name'
  end

  it "can be deleted by editors" do
    count = Personality.count
    create_personality_document('Delname', 'Delsurname')
    Personality.count.should eq(count+1)
    click_action_link :drafts
    page.should have_content 'Delname'
    find('a', :text => 'Delname').click
    click_action_link :delete
    click_action_link :drafts
    page.should_not have_content 'Delname'
    visit_section :personalities
    page.should_not have_content 'Delname'
    Personality.count.should eq(count)
  end

  it "can be edited" do
    create_personality_document 'Origname', 'Origsurname'
    click_action_link :drafts
    find('a', :text => 'Origname').click
    page.should have_css 'form.edit_personality'
    fill_in 'personality_name', :with => 'Editedname'
    fill_in 'personality_surname', :with => 'Editedsurname'
    save_document
    verify_confirmation_message
    click_action_link :drafts
    page.should have_content 'Editedsurname, Editedname'
    page.should_not have_content 'Origname'
  end

  it "can be published and unpublished" do
    create_personality_document 'Publiname', 'Publisurname'
    click_action_link :drafts
    find('a', :text => 'Publiname').click
    click_action_link :publish
    verify_confirmation_message
    click_action_link :drafts
    page.should_not have_content 'Publiname'
    visit_section :personalities
    page.should have_content 'Publiname'
    find('a', :text => 'Publiname').click
    page.should have_content 'Publiname'

    click_action_link :unpublish
    verify_confirmation_message
    click_action_link :drafts
    page.should have_content 'Publiname'
    visit_section :personalities
    page.should_not have_content 'Publiname'
  end


  it "can have photos attached to them", :js => true do
    visit_section :personalities
    click_on('new_document')
    page.should have_css 'form#new_personality'
    fill_in 'personality_name', :with => 'Photoname'
    fill_in 'personality_surname', :with => 'Photosurname'
    attach_file 'new_photo_1', "#{File.dirname(__FILE__)}/../test-data/photo-1.jpg"
    attach_file 'new_photo_2', "#{File.dirname(__FILE__)}/../test-data/photo-2.jpg"
    attach_file 'new_photo_3', "#{File.dirname(__FILE__)}/../test-data/photo-3.jpg"
    attach_file 'new_photo_4', "#{File.dirname(__FILE__)}/../test-data/photo-4.jpg"
    attach_file 'new_photo_5', "#{File.dirname(__FILE__)}/../test-data/photo-5.jpg"
    save_document
    verify_confirmation_message
    click_action_link :publish
    visit_section :personalities
    find('a', :text => 'Photoname').click
    all('.photos .gallery a img').count.should eq(5)

    click_action_link :edit
    all('#edit-photos a img').count.should eq(5)
    attach_file 'new_photo_1', "#{File.dirname(__FILE__)}/../test-data/photo-6.jpg"
    attach_file 'new_photo_2', "#{File.dirname(__FILE__)}/../test-data/photo-7.jpg"
    save_document
    verify_confirmation_message
    visit_section :personalities
    find('a', :text => 'Photoname').click
    all('.photos .gallery a img').count.should eq(7)

    click_action_link :edit
    all('#edit-photos a img').count.should eq(7)
    find('#edit-photos button.delete.photo').click
    find('input[rel="delete"][value="1"]')  # wait for the deletion JS action to take effect
    save_document
    verify_confirmation_message
    all('#edit-photos a img').count.should eq(6)
    visit_section :personalities
    find('a', :text => 'Photoname').click
    all('.photos .gallery a img').count.should eq(6)
  end

  it "display searched-for personalities" do
    visit_section :personalities
    fill_in 'search', :with => 'SomeNonExistentSearchTerm'
    find('#search_button').click
    all('a[rel="document"]').count.should eq(0)
    page.should have_css '.not-found'

    Personality.create!(:name => 'DocumentForSearch', :surname => 'whatever', :author => User.first, :categories => [])
    ThinkingSphinx::Test.index; sleep(0.5)
    fill_in 'search', :with => 'DocumentForSearch'
    find('#search_button').click
    all('a[rel="document"]').count.should eq(1)
    page.should_not have_css '.not-found'
  end
end