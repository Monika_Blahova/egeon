class CreateDestinations < ActiveRecord::Migration
  def change
    create_table :destinations do |t|
      t.string :title
      t.string :title_ordering
      t.string :motivation
      t.text :description

      t.date :last_checkup
      t.text :basic_numeral_data
      t.text :protected_teritory_categories

      # contact info
      t.string :website
      t.string :email
      t.string :telephone
      t.string :contact_notes

      # location
      t.string :cadastre
      t.text :coordinates
      t.string :municipality
      t.string :municipality_section
      t.string :postal_number
      t.string :street
      t.string :street_number
      t.float :elevation

      # access
      t.text :walking_access
      t.text :biking_access
      t.text :bus_access
      t.text :car_access
      t.text :train_access
      t.text :public_transport_access
      t.text :for_disabled
      t.text :tips_suggestions

      t.timestamps
    end
  end
end
