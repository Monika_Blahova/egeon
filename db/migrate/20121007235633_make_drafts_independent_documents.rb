class MakeDraftsIndependentDocuments < ActiveRecord::Migration

  # This publishes the document (all documents created up till now are considered public)
  # and merges any drafts back with their sources
  def migrate_document!(doc)
      model = doc.class
      source = doc.source if doc.respond_to? :source
      if source.present?
        attributes = Set.new(model.attribute_names) - model.protected_attributes
        attributes.each { |attr| source.send(attr + '=', doc.send(attr)) }
        source.is_draft = false
        source.save!
        doc.destroy
      else
        doc.is_draft = false
        doc.save!
      end
  end

  def up
    models = [Destination, Route, Event, Term, Personality]
    models.each do |model|
      documents = model.all
      documents.each { |doc| migrate_document! doc }
    end

    tables = [:destinations, :routes, :events, :terms, :personalities]
    tables.each { |table| remove_column table, :source_id }
  end

  def down
    tables = [:destinations, :routes, :events, :terms, :personalities]
    tables.each { |table| add_column table, :source_id, :integer, :default => nil }
  end
end
