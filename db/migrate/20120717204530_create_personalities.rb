class CreatePersonalities < ActiveRecord::Migration
  def change
    create_table :personalities do |t|
      t.string :name
      t.string :surname
      t.string :motivation
      t.date :born
      t.date :died
      t.string :birthplace
      t.string :deathplace
      t.text :description
      t.timestamps
    end
  end
end
