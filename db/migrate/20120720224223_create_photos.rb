class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.text :description
      t.string :content_type
      t.string :filename
      t.string :thumbnail
      t.string :attribution

      t.timestamps
    end

    create_table :document_photos do |t|
      t.references :document, :polymorphic => true
      t.integer :photo_id, :null => false
    end
  end
end
