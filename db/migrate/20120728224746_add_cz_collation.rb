class AddCzCollation < ActiveRecord::Migration
  def up
    execute "create collation cz (locale='cs_CZ.utf8');"
    remove_column :destinations, :title_ordering
  end

  def down
    execute "drop collation cz;"
    raise ActiveRecord::IrreversibleMigration
  end
end
