class AddDraftsToDocuments < ActiveRecord::Migration
  def change
    change_table :destinations do |t|
      t.integer :source_id, :null => true
    end
    change_table :events do |t|
      t.integer :source_id, :null => true
    end
    change_table :personalities do |t|
      t.integer :source_id, :null => true
    end
    change_table :routes do |t|
      t.integer :source_id, :null => true
    end
    change_table :terms do |t|
      t.integer :source_id, :null => true
    end
  end
end
