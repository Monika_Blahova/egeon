class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.integer :destination_id, :null => false

      t.string :service
      t.string :price
      t.string :notes
    end
  end
end
