class AddPaperclipToPhotos < ActiveRecord::Migration
  def up
    add_attachment :photos, :image
    remove_column :photos, :content_type
    remove_column :photos, :filename
    remove_column :photos, :thumbnail
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
