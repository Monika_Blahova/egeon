class AddIconsToRouteSectionEntries < ActiveRecord::Migration
  def up
    remove_column :route_section_entries, :map_marks
    remove_column :route_section_entries, :next_direction
    remove_column :route_section_entries, :bike_accessibility

    add_attachment :route_section_entries, :map_marks
    add_attachment :route_section_entries, :next_direction
    add_attachment :route_section_entries, :bike_accessibility
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
