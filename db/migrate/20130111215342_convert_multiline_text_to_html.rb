class ConvertMultilineTextToHtml < ActiveRecord::Migration

  def text_to_html(text)
    return '' if text.blank?
    paragraphs = text.split("\n").map(&:strip)
    paragraphs.map { |p| Haml::Helpers::html_escape(p) }.join('<br>')
  end

  def up
    Destination.all.each do |d|
      d.description = text_to_html(d.description)
      if d.lodging.present?
        d.lodging.general_notes = text_to_html(d.lodging.general_notes)
        d.lodging.room_description = text_to_html(d.lodging.room_description)
        d.lodging.services_included = text_to_html(d.lodging.services_included)
        d.lodging.services_available = text_to_html(d.lodging.services_available)
        d.lodging.internet_availability = text_to_html(d.lodging.internet_availability)
        d.lodging.arrival_since = text_to_html(d.lodging.arrival_since)
        d.lodging.departure_till = text_to_html(d.lodging.departure_till)
        d.lodging.children_extra_beds = text_to_html(d.lodging.children_extra_beds)
        d.lodging.animals = text_to_html(d.lodging.animals)
        d.lodging.bookings_payments_cancellation = text_to_html(d.lodging.bookings_payments_cancellation)
      end
      d.walking_access = text_to_html(d.walking_access)
      d.biking_access = text_to_html(d.biking_access)
      d.train_access = text_to_html(d.train_access)
      d.public_transport_access = text_to_html(d.public_transport_access)
      d.car_access = text_to_html(d.car_access)
      d.bus_access = text_to_html(d.bus_access)
      d.for_disabled = text_to_html(d.for_disabled)
      d.tips_suggestions = text_to_html(d.tips_suggestions)
      d.save!
    end

    Route.all.each do |d|
      d.description = text_to_html(d.description)
      d.save!
    end

    Event.all.each do |d|
      d.description = text_to_html(d.description)
      d.save!
    end

    Term.all.each do |d|
      d.description = text_to_html(d.description)
      d.save!
    end

    Personality.all.each do |d|
      d.description = text_to_html(d.description)
      d.save!
    end
  end

  def down
  end
end
