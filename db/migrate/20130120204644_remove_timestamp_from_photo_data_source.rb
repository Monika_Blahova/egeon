require 'fileutils'

class RemoveTimestampFromPhotoDataSource < ActiveRecord::Migration
  def up
    original_hash_data = ':class/:attachment/:id/:style/:updated_at'
    new_hash_data = ':class/:attachment/:id/:style'
    Photo.all.each do |photo|
      i = photo.image
      already_updated = (i.options[:hash_data] == new_hash_data) && i.exists?
      if !already_updated
        puts "Updating photo #{photo.id}"
        i.options[:hash_data] = original_hash_data
        original_full_url = i.url
        original_thumb_url = i.url(:thumb)
        raise "Image for photo: #{photo.id}, url: #{original_full_url} doesn't exist" unless i.exists?

        i.options[:hash_data] = new_hash_data
        new_full_url = i.url
        new_thumb_url = i.url(:thumb)

        original_full_path = (Rails.root + 'public').to_s + URI(original_full_url).path
        original_thumb_path = (Rails.root + 'public').to_s + URI(original_thumb_url).path

        new_full_path = (Rails.root + 'public').to_s + URI(new_full_url).path
        new_thumb_path = (Rails.root + 'public').to_s + URI(new_thumb_url).path

        FileUtils.cp(original_full_path, new_full_path)
        FileUtils.cp(original_thumb_path, new_thumb_path)

        raise "Image for photo: #{photo.id} not copied successfully" unless  i.exists?

        i.reprocess!

        raise "Image for photo: #{photo.id} not reprocessed successfully" unless  i.exists?

        photo.image = i
        photo.save!
        FileUtils.rm(original_full_path)
        FileUtils.rm(original_thumb_path)
      end
    end
  end

  def down
  end
end
