class ChangeElevationToText < ActiveRecord::Migration
  def change
    change_column :destinations, :elevation, :text
  end
end
