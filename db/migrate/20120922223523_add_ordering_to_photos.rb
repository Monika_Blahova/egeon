class AddOrderingToPhotos < ActiveRecord::Migration
  def change
    add_column :document_photos, :index, :integer
  end
end
