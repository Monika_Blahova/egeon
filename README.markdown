Geoinfotour
===========

## Installation

1. Create `database.yml`:

```
$ cp config/database.yml.sample config/database.yml
```

2. Set up a good password for production

3. Install [ImageMagick](http://www.imagemagick.org/)

4. Install [PostgreSLQ](http://www.postgresql.org/) (version 9.1 or higher -- we're using the new collation
   features)

5. Set up PostgreSQL, enter the previously set up production password:

```
$ sudo su postgres
$ psql template1
template1=# create role geoinfotour with createdb login password '$PASSWORD';
template1=# select * from pg_user;    # Verify user created
template1=# exit
```

6. Add these lines to `/etc/postgresql/9.1/main/pg_hba.conf` before `local   all
all                                     peer`:

```
local postgres geoinfotour md5
local "geoinfotour" geoinfotour md5
# The following two are for dev and test purposes:
local "geoinfotour_test" geoinfotour md5
local "geoinfotour_development" geoinfotour md5
```


7. Restart PostgreSQL:

```
$ sudo service postgresql restart
```

8. Create the database:

```
$ RAILS_ENV=production bundle exec rake db:create
$ RAILS_ENV=production bundle exec rake db:migrate
$ RAILS_ENV=production bundle exec rake db:seed
```

9. Run the server!

```
$ RAILS_ENV=production bundle exec thin start
```

## Adding new users

    $ bundle exec rails c production
    > u = User.new
    > u.email = 'jdoe@example.com'
    > u.name = 'John Doe'
    > u.password = rand(36**20).to_s(36)
    > u.save!
    > u.send_reset_password_instructions rescue nil
    > "http://egeon.cz/users/password/edit?reset_password_token=#{u.reset_password_token}"


## Docker ##

1. Build the database:

    # docker build -t egeon/postgres dockerfile_database

2. Start the database container:

    # docker run --name egeon-database -d egeon/postgres

3. Build the egeon image:

    # docker build -t egeon/app .

4. Run migrations:

    # docker run --rm --link egeon-database:postgres egeon/app /run_migrations.sh

5. Create the user:

    # docker run --rm -i -t --link egeon-database:postgres egeon/app bundle exec rails console
    > u = User.new :email => 'jdoe@example.com', :name => 'John Doe'
    > u.password = rand(36**20).to_s(36)
    > u.save!
    > u.send_reset_password_instructions rescue nil
    > "http://localhost:3000/users/password/edit?reset_password_token=#{u.reset_password_token}"


6. Run the egeon web app:

    # docker run -t -i --rm -p 3000:3000 --link egeon-database:postgres egeon/app bundle exec rails server
