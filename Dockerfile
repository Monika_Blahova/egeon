FROM centos:7

RUN yum install -y epel-release && yum -y update && yum -y install ruby rubygem-bundler ImageMagick nodejs

WORKDIR /var/lib/egeon/

# Install deps
ADD Gemfile /var/lib/egeon/Gemfile
ADD Gemfile.lock /var/lib/egeon/Gemfile.lock

RUN yum -y install postgresql-devel libxslt-devel ruby-devel make gcc gcc-c++ libxml2-devel && bundle install --without test development --path .gems
# && yum remove -y libxslt-devel libgcrypt-devel libgpg-error-devel make gcc gcc-c++ libxml2-devel ruby-devel xz-devel zlib-devel libstdc++-devel postgresql-devel postgresql postgresql-libs glibc-devel

# Script to set up the database
ADD run_migrations.sh /run_migrations.sh

ENV HOME /var/lib/egeon/
RUN useradd --no-create-home --home-dir $HOME egeon

# Copy the app stuff to the container:
ADD app /var/lib/egeon/app
ADD db /var/lib/egeon/db
ADD config /var/lib/egeon/config
ADD Rakefile /var/lib/egeon/Rakefile
ADD config.ru /var/lib/egeon/config.ru
ADD public /var/lib/egeon/public
ADD vendor /var/lib/egeon/vendor
ADD script /var/lib/egeon/script

# Database config
ADD config/database.yml.docker /var/lib/egeon/config/database.yml

# TODO: S3 config

RUN /usr/bin/chown --recursive egeon:egeon $HOME
USER egeon

ENV RAILS_ENV production

EXPOSE 3000
