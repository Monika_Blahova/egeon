module DocumentsHelper

  def draft?
    is_draft
  end

  def author_cant_be_coauthor
    if coauthors.include? author
      errors.add(:author, "cannot be coauthor at the same time")
    end
  end

  def categories
    document_categories.map {|dc| dc.category}.freeze
  end

  def top_level_section
    Category.find_by_name self.class.name.tableize
  end

  def has_category?(category_id)
    if new_record?
      categories.select {|c| c.id == category_id}.present?
    else
      DocumentCategory.find_by_category_id_and_document_id(category_id, id).present?
    end
  end

  def update_categories(ids)
    ids ||= []
    ids = ids.map(&:to_i)
    ids << top_level_section.id
    document_categories.clear
    new_categories = ids.map {|id| Category.find(id).path}.flatten.compact.uniq
    new_categories.each do |c|
      dc = DocumentCategory.new
      dc.category_id = c.id
      document_categories << dc
    end
  end

  def categories=(ids)
    update_categories(ids)
  end

  def belongs_to_section
    sections = categories.select &:root?

    if sections.count == 0
      errors.add(:document, "missing top-level section")
    end

    if sections.count > 1
      errors.add(:document, "has more than one top-level section")
    end

  end

  def normalized_title
    collated = ActiveSupport::Inflector.transliterate(title)
    collated.downcase.gsub(/[^a-z ]/, '').gsub(/ \s*/, '-')
  end

  def belongs_to_category
    return if draft?
    # TODO: seems that Terms don't have categories. Figure out how to handle this.
    # Probably move the category validation to each individual model
    # if categories.length < 2
    #   puts "CATEGORIES (#{categories.length})", categories.inspect
    #   errors.add(:document, "must belong to at least one category")
    # end
  end

end
