class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!

  def current_category
    Category.find_by_name(params[:category_name]) || Category.find(params[:c]) || Category.find_by_name(params[:controller])
  end

  class Breadcrumb
    attr_accessor :name
    def initialize(name)
      self.name = name
    end
  end

  def recursively_blank?(hash)
    return true if hash.blank?
    return false unless hash.respond_to? :values

    return hash.values.all? {|v| recursively_blank? v }
  end

  def documents_index(params, drafts=false, order_statement='(title collate "cz")')
    @current_category = current_category
    @breadcrumbs = @current_category.path
    if params.include? :q
      current_model = request.path_parameters[:controller].singularize.capitalize.safe_constantize
      category_ids = Set.new(@current_category.document_ids)
      docs = current_model.search(params[:q]).select do |doc|
        category_ids.include? doc.id
      end
      @documents = current_model.where(:id => docs).order(order_statement).page(params[:p])
      @breadcrumbs << Breadcrumb.new(t(:search_breadcrumb, :terms => params[:q]))
    else
      published_docs = @current_category.documents(order_statement).where(:is_draft => drafts)
      @documents = published_docs.page(params[:p])
    end

    if drafts
      flash.now[:notice] = t(:list_drafts)
    end
  end

  def document_show(document_class, params)
    @current_category = current_category
    @doc = document_class.find params[:id]
    raise ActiveRecord::RecordNotFound if (@doc.draft? && current_user.blank?)
    unless params[:title] == @doc.normalized_title
      flash.keep
      redirect_to "/#{t 'url_routing.' + @current_category.top_section.name}/#{@doc.id}/#{@doc.normalized_title}"
      return
    end
    @breadcrumbs = @current_category.path
    @breadcrumbs << Breadcrumb.new(@doc.title)
  end

  def create_new_document!(document_class, params)
    attributes = params[document_class.to_s.downcase]
    doc = document_class.new attributes
    doc.author = current_user
    doc.update_categories(params[:categories])
    doc.is_draft = true
    doc.save!

    new_photos = upload_new_photos(params[:new_photos])
    new_photos.each { |photo| doc.photos << photo }

    return doc
  end

  def upload_new_photos(photo_params)
    photo_params ||= []
    uploaded_photos = []
    photo_params.each do |file|
      photo = Photo.new
      photo.image = file
      uploaded_photos << photo if photo.image.present?
    end

    return uploaded_photos
  end

  def update_document!(doc, params)
    document_class = doc.class

    doc.author = current_user
    doc.update_categories(params[:categories])

    doc.update_attributes! params[document_class.to_s.downcase]

    (params[:photos] || {}).each do |id, values|
      if values[:delete].to_i == 1
        Photo.find(id).destroy
      else
        photo = Photo.find(id)
        values.delete(:delete)
        photo.update_attributes! values
      end
    end

    # Records the current order of the photos into the document_photos table
    document_photo_mappings = Hash[*doc.document_photos.map{|dp| [dp.photo_id, dp]}.flatten]
    (params[:photo_order] || []).each_with_index do |photo_id, index|
      photo_id = photo_id.to_i
      document_photo = document_photo_mappings[photo_id]
      if document_photo
        document_photo.index = index
        document_photo.save!
      end
    end

    new_photos = upload_new_photos(params[:new_photos])
    new_photos.each { |photo| doc.photos << photo }

    return doc
  end

end
