class RoutesController < ApplicationController
  def index
    documents_index(params)
  end

  def drafts
    documents_index(params, drafts=true)
    @breadcrumbs << Breadcrumb.new(t :list_drafts)
    render :index
  end

  def show
    document_show(Route, params)
  end

  def new
    @doc = Route.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def create
    sanitize_params! params
    doc = create_new_document!(Route, params)

    params[:route_section_entries].each do |vals|
      doc.route_section_entries << RouteSectionEntry.new(vals)
    end

    flash[:notice] = t(:document_created)
    redirect_to edit_route_path(doc)
  end

  def edit
    @doc = Route.find params[:id]
    @current_category = current_category
    @breadcrumbs = []
  end

  def update
    sanitize_params! params
    doc = Route.find(params[:id])
    update_document!(doc, params)

    Route.transaction do
      # Replace old route section entries with the new ones
      doc.route_section_entries.each { |e| e.destroy }
      params[:route_section_entries].each do |vals|
        doc.route_section_entries << RouteSectionEntry.new(vals)
      end
    end

    flash[:notice] = t(:document_saved)
    redirect_to edit_route_path(doc)
  end

  def publish
    doc = Route.find params[:id]
    doc.is_draft = false
    doc.save!
    flash[:notice] = t(:document_published, :name => doc.title)
    redirect_to routes_path(doc)
  end

  def unpublish
    doc = Route.find params[:id]
    doc.is_draft = true
    doc.save!
    flash[:notice] = t(:document_unpublished, :name => doc.title)
    redirect_to routes_drafts_path
  end

  def destroy
    doc = Route.find params[:id]
    doc.destroy
    redirect_to routes_path
  end

  private

  def sanitize_params!(params)
    params[:categories] ||= []

    params[:route_section_entries] ||= []
    params[:route_section_entries].reject! { |vals| recursively_blank? vals }

    params[:route_section_entries].each do |e|
      e[:distance] = e[:distance].to_f
      e[:elevation] = e[:elevation].to_f
    end
  end
end
