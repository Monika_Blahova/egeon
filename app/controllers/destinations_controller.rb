class DestinationsController < ApplicationController
  def index
    documents_index(params)
  end

  def drafts
    documents_index(params, drafts=true)
    @breadcrumbs << Breadcrumb.new(t :list_drafts)
    render :index
  end

  def show
    document_show(Destination, params)
    if @doc.latitude && @doc.longitude
      @maps = {
        "circles" => {
          "data" => JSON.dump([{
            :lat => @doc.latitude,
            :lng => @doc.longitude,
            :radius => 1500,  # 1.5km circle, the map covers ~5km area vertically
          }]),
          "options" => {
            "fillOpacity" => 0,
            "strokeOpacity" => 0,
          },
        },
        "markers" => {
          "data" => JSON.dump([{
            :lat => @doc.latitude,
            :lng => @doc.longitude,
            :title => @doc.gmaps4rails_title,
            :description => @doc.gmaps4rails_infowindow
          }]),
        },
      }
    end
  end

  def new
    @doc = Destination.new
    @doc.lodging = Lodging.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def create
    sanitize_params! params
    doc = create_new_document!(Destination, params)

    if params[:lodging]
      doc.lodging = Lodging.new params[:lodging]
      doc.lodging.save!
    end

    params[:open_hours].each { |vals| doc.open_hours << OpenHour.new(vals) }
    params[:services].each { |vals| doc.services << Service.new(vals) }

    flash[:notice] = t(:document_created)
    redirect_to edit_destination_path(doc)
  end

  def edit
    @doc = Destination.find params[:id]
    @doc.lodging ||= Lodging.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def update
    sanitize_params! params
    doc = Destination.find params[:id]
    update_document!(doc, params)

    if params[:lodging]
      doc.lodging ||= Lodging.new
      doc.lodging.update_attributes! params[:lodging]
    elsif doc.lodging
      doc.lodging.destroy
    end

    # Replace old open hour entries with the new ones
    open_hours = params[:open_hours].map {|values| OpenHour.new values}
    doc.open_hours.each {|oh| oh.destroy}
    doc.open_hours = open_hours

    # Replace the old service entries with the new ones
    services = params[:services].map {|values| Service.new values}
    doc.services.each {|s| s.destroy}
    doc.services = services

    flash[:notice] = t(:document_saved)
    redirect_to edit_destination_path(doc)
  end

  def publish
    doc = Destination.find params[:id]
    doc.is_draft = false
    doc.save!
    flash[:notice] = t(:document_published, :name => doc.title)
    redirect_to destination_path(doc)
  end

  def unpublish
    doc = Destination.find params[:id]
    doc.is_draft = true
    doc.save!
    flash[:notice] = t(:document_unpublished, :name => doc.title)
    redirect_to destinations_drafts_path
  end

  def destroy
    doc = Destination.find params[:id]
    doc.destroy
    redirect_to destinations_path
  end

  private

  def sanitize_params!(params)
    params[:categories] ||= []
    params[:lodging][:accept_credit_cards] = !params[:lodging][:accept_credit_cards].to_i.zero?
    params.delete(:lodging) if recursively_blank?(params[:lodging])

    params[:open_hours] ||= []
    params[:open_hours].reject! {|vals| recursively_blank? vals}

    params[:services] ||= []
    params[:services].reject! {|vals| recursively_blank? vals}

    params[:photos] ||= {}
  end
end
