class MapsController < ApplicationController
  def index
    if params[:id].present?
      doc = Destination.find(params[:id])
      @map_markers = doc.to_gmaps4rails
    else
      @map_markers = Destination.public.to_gmaps4rails
    end
  end
end
