class EventsController < ApplicationController
  def index
    documents_index(params)
  end

  def drafts
    documents_index(params, drafts=true)
    @breadcrumbs << Breadcrumb.new(t :list_drafts)
    render :index
  end

  def show
    document_show(Event, params)
  end

  def new
    @doc = Event.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def create
    doc = create_new_document!(Event, params)
    flash[:notice] = t(:document_created)
    redirect_to edit_event_path(doc)
  end

  def edit
    @doc = Event.find params[:id]
    @current_category = current_category
    @breadcrumbs = []
  end

  def update
    doc = Event.find(params[:id])
    update_document!(doc, params)
    flash[:notice] = t(:document_saved)
    redirect_to edit_event_path(doc)
  end

  def publish
    doc = Event.find params[:id]
    doc.is_draft = false
    doc.save!
    flash[:notice] = t(:document_published, :name => doc.title)
    redirect_to event_path(doc)
  end

  def unpublish
    doc = Event.find params[:id]
    doc.is_draft = true
    doc.save!
    flash[:notice] = t(:document_unpublished, :name => doc.title)
    redirect_to events_drafts_path
  end

  def destroy
    doc = Event.find params[:id]
    doc.destroy
    redirect_to events_path
  end
end
