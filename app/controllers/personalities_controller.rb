class PersonalitiesController < ApplicationController
  def index
    documents_index(params, drafts=false, '(surname collate "cz", name collate "cz")')
  end

  def drafts
    documents_index(params, drafts=true, '(surname collate "cz", name collate "cz")')
    @breadcrumbs << Breadcrumb.new(t :list_drafts)
    render :index
  end

  def show
    document_show(Personality, params)
  end

  def new
    @doc = Personality.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def create
    doc = create_new_document!(Personality, params)
    flash[:notice] = t(:document_created)
    redirect_to edit_personality_path(doc)
  end

  def edit
    @doc = Personality.find params[:id]
    @current_category = current_category
    @breadcrumbs = []
  end

  def update
    doc = Personality.find(params[:id])
    update_document!(doc, params)
    flash[:notice] = t(:document_saved)
    redirect_to edit_personality_path(doc)
  end

  def publish
    doc = Personality.find params[:id]
    doc.is_draft = false
    doc.save!
    flash[:notice] = t(:document_published, :name => doc.title)
    redirect_to personality_path(doc)
  end

  def unpublish
    doc = Personality.find params[:id]
    doc.is_draft = true
    doc.save!
    flash[:notice] = t(:document_unpublished, :name => doc.title)
    redirect_to personalities_drafts_path
  end

  def destroy
    doc = Personality.find params[:id]
    doc.destroy
    redirect_to personalities_path
  end
end
