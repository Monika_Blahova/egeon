class TermsController < ApplicationController
  def index
    documents_index(params)
  end

  def show
    document_show(Term, params)
  end

  def drafts
    documents_index(params, drafts=true)
    @breadcrumbs << Breadcrumb.new(t :list_drafts)
    render :index
  end

  def new
    @doc = Term.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def create
    doc = create_new_document!(Term, params)
    flash[:notice] = t(:document_created)
    redirect_to edit_term_path(doc)
  end

  def edit
    @doc = Term.find params[:id]
    @current_category = current_category
    @breadcrumbs = []
  end

  def update
    doc = Term.find(params[:id])
    update_document!(doc, params)
    flash[:notice] = t(:document_saved)

    redirect_to edit_term_path(doc)
  end

  def publish
    doc = Term.find params[:id]
    doc.is_draft = false
    doc.save!
    flash[:notice] = t(:document_published, :name => doc.title)
    redirect_to term_path(doc)
  end

  def unpublish
    doc = Term.find params[:id]
    doc.is_draft = true
    doc.save!
    flash[:notice] = t(:document_unpublished, :name => doc.title)
    redirect_to terms_drafts_path
  end

  def destroy
    doc = Term.find params[:id]
    doc.destroy
    redirect_to terms_path
  end
end
