class Photo < ActiveRecord::Base
  has_many :document_photos
  attr_accessible :description, :attribution

  has_attached_file :image, {
    :path => "/photos/:hash.:extension",
    :url => ":s3_domain_url",
    :hash_data => ':class/:attachment/:id/:style',
    :hash_secret => "whatever",
    :storage => :s3,
    :s3_credentials => "config/s3_credentials.yaml",
    :styles => {
      :thumb => ["x300", :jpg],
      :lightbox => ["950x700>", :jpg],
    },
  }

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def documents
    document_photos.map &:document
  end
end
