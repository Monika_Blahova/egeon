class Lodging < ActiveRecord::Base
  belongs_to :destination
  attr_protected :destination_id


  def non_empty?
    ["general_notes", "room_description", "services_included",
      "services_available", "internet_availability", "arrival_since",
      "departure_till", "children_extra_beds", "animals",
      "bookings_payments_cancellation", "accept_credit_cards"].any? do |a|
        send(a).present?
      end
  end

end
