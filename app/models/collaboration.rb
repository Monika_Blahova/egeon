class Collaboration < ActiveRecord::Base
  belongs_to :document, :polymorphic => true
  belongs_to :user
end
