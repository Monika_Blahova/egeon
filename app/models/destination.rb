# encoding: utf-8

class Destination < ActiveRecord::Base
  has_one :lodging, :dependent => :destroy
  has_many :services, :dependent => :destroy
  has_many :open_hours, :dependent => :destroy

  attr_protected :source_id, :author_id, :created_at, :updated_at

  validates :title, :presence => true

  acts_as_gmappable :process_geocoding => false

  scope :public, where(:is_draft => false)
  scope :draft, where(:is_draft => true)

  # Each document must have one author and may have one or more coauthors
  belongs_to :author, :class_name => 'User', :foreign_key => 'author_id'
  has_many :collaborations, :as => :document, :dependent => :destroy
  has_many :coauthors, :through => :collaborations, :source => 'user'
  validates :author, :presence => true
  validate :author_cant_be_coauthor

  # Each document may have one or more photos
  has_many :document_photos, :as => :document, :dependent => :destroy
  has_many :photos, :through => :document_photos, :order => :index

  # Documents have categories associated with them
  has_many :document_categories, :as => :document, :dependent => :destroy
  validate :belongs_to_section
  validate :belongs_to_category

  include ApplicationHelper
  include DocumentsHelper

  def address_present?
    attributes_present?([:street, :municipality_section, :postal_number])
  end

  def contact_info_present?
    attributes_present? [:telephone, :website, :email, :contact_notes]
  end

  def location_present?
    address_present? || attributes_present?([:cadastre, :coordinates, :elevation,
      :basic_numeral_data, :protected_teritory_categories])
  end

  def lodging_present?
    lodging.present? && lodging.non_empty?
  end

  def access_present?
    attributes_present? [:walking_access, :biking_access, :bus_access,
      :car_access, :train_access, :public_transport_access, :for_disabled]
  end

  def latitude
    coords = parse_coordinates(coordinates)
    coords[:latitude] if coords
  end

  def longitude
    coords = parse_coordinates(coordinates)
    coords[:longitude] if coords
  end

  def gmaps4rails_infowindow
    href = document_show_url(self)
    photo = if photos.present?
      %Q(<a class="photo"><img src="#{photos.first.image.url(:thumb)}" height="10px"></img></a>)
    else
      ""
    end
    infobox = <<END
    #{photo}
    <strong>#{title}</strong>
    <p style="max-width: 400px">#{motivation}</p>
    <a href="#{href}">#{I18n::t :show_details}</a>
END
  end

  def gmaps4rails_title
    title
  end

  private

  def attributes_present?(attributes)
    attributes.any? do |a|
      send(a).present?
    end
  end

  def parse_coordinates(coordinates)
    # format: 49°17'52.197"N, 16°40'21.780"E
    rx = /(\d+)°(\d+).(\d+\.\d+)[^\d]*(\d+)°(\d+).(\d+\.\d+)/
    m = coordinates.match(rx)
    if m
      lat = m[1].to_f + (m[2].to_f / 60) + (m[3].to_f / 3600)
      lon = m[4].to_f + (m[5].to_f / 60) + (m[6].to_f / 3600)
      {
        :latitude => lat,
        :longitude => lon,
      }
    end
  end

end
