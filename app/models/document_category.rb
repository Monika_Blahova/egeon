class DocumentCategory < ActiveRecord::Base
  belongs_to :document, :polymorphic => true
  belongs_to :category

  def category
    Category.find(category_id)
  end
end
