class RouteSectionEntry < ActiveRecord::Base
  belongs_to :route
  attr_protected :route_id

  has_attached_file :map_marks, {
    :url => "/photos/:hash.:extension",
    :hash_data => ':class/:attachment/:style',
    :hash_secret => "whatever",
    :preserve_files => true,
    :styles => {
      :icon => ["32x32", :jpg],
    },
  }

  has_attached_file :next_direction, {
    :url => "/photos/:hash.:extension",
    :hash_data => ':class/:attachment/:style',
    :hash_secret => "whatever",
    :preserve_files => true,
    :styles => {
      :icon => ["32x32", :jpg],
    },
  }

  has_attached_file :bike_accessibility, {
    :url => "/photos/:hash.:extension",
    :hash_data => ':class/:attachment/:style',
    :hash_secret => "whatever",
    :preserve_files => true,
    :styles => {
      :icon => ["32x32", :jpg],
    },
  }


end
